#!/bin/sh
# Copyright (C) 2020 Stephan Kreutzer
#
# This file is part of media-curation.
#
# media-curation is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3 or any later
# version of the license, as published by the Free Software Foundation.
#
# media-curation is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with media-curation. If not, see <http://www.gnu.org/licenses/>.

java -cp ./digital_publishing_workflow_tools/csv_to_xml/csv_to_xml_1/ csv_to_xml_1 ./jobfile_csv_to_xml_1.xml ./resultinfo_csv_to_xml_1.xml
java -cp ./prepare_input_1/ prepare_input_1 input.xml input_prepared.xml

rm -r ./clips/
mkdir -p ./clips/
rm ./cut.sh
java -cp ./digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/ xml_xslt_transformator_1 ./jobfile_xml_xslt_transformator_1.xml ./resultinfo_xml_xslt_transformator_1.xml
chmod 755 ./cut.sh
./cut.sh
