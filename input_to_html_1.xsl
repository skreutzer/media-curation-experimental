<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2020 Stephan Kreutzer

This file is part of media-curation.

media-curation is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

media-curation is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with media-curation. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"/>

  <xsl:template match="/">
    <html version="-//W3C//DTD XHTML 1.1//EN" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd" xml:lang="en" lang="en">
      <head>
        <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8"/>
<xsl:text>&#x0A;</xsl:text>
<xsl:comment> This file was created by input_to_html_1.xsl of media-curation, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/skreutzer/media-curation-experimental/). </xsl:comment>
<xsl:text>&#x0A;</xsl:text>
        <title>Meeting</title>
        <script type="text/javascript">
          "use strict";

        </script>
        <style type="text/css">
          body
          {
              font-family: sans-serif;
          }

          .entry
          {
              border: 1px solid #000000;
              padding: 5px;
              margin: 5px;
          }
        </style>
      </head>
      <body>
        <div>
          <xsl:apply-templates select=".//statement"/>
        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="//statement">
    <xsl:apply-templates select="./text"/>
  </xsl:template>

  <xsl:template match="//statement/text">
    <div class="entry">
      <div>
        <xsl:apply-templates select=".//text()"/>
      </div>
      <div>
        <audio controls="controls">
          <source type="audio/ogg">
            <xsl:attribute name="src">
              <xsl:number count="*[self::statement]" level="any"/>
              <xsl:text>.ogg</xsl:text>
            </xsl:attribute>
            <a>
              <xsl:attribute name="src">
                <xsl:number count="*[self::statement]" level="any"/>
                <xsl:text>.ogv</xsl:text>
              </xsl:attribute>
              <xsl:text>play</xsl:text>
            </a>
          </source>
        </audio>
      </div>
    </div>
  </xsl:template>

  <xsl:template match="//text/text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="node()|@*|text()"/>

</xsl:stylesheet>
