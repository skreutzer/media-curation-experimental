<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2020 Stephan Kreutzer

This file is part of media-curation.

media-curation is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

media-curation is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with media-curation. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:anno="htx-scheme-id://org.hypertext-systems.20180702T071630Z/clients/org.w3.19940706T040000Z/ns/anno.jsonld.20190420T105838Z" xmlns:mf="htx-scheme-id://org.hypertext-systems.20180702T071630Z/clients/org.w3.19940706T040000Z/TR/media-frags.20120925T000000Z">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no"/>

  <xsl:template match="/">
    <xsl:comment> This file was created by input_to_web_annotation_1.xsl of media-curation, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/skreutzer/media-curation-experimental/). </xsl:comment>
    <annotations>
      <xsl:apply-templates select=".//statement"/>
    </annotations>
  </xsl:template>

  <xsl:template match="//statement">
    <annotation>
      <anno:type>Annotation</anno:type>
      <anno:body>
        <anno:type>TextualBody</anno:type>
        <anno:format>text/plain</anno:format>
        <anno:language>en</anno:language>
        <anno:value>
          <xsl:value-of select="./text/text()"/>
        </anno:value>
      </anno:body>
      <anno:target>
        <anno:source>TODO</anno:source>
        <anno:selector>
          <anno:type>FragmentSelector</anno:type>
          <anno:conformsTo>http://www.w3.org/TR/media-frags/</anno:conformsTo>
          <anno:value>
            <xsl:text>t=</xsl:text>
            <xsl:value-of select="./start//text()"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="./end//text()"/>
          </anno:value>
          <xsl:comment> Media Fragment in an URI as Web Annotation selector is pretty bad for parsing/markup (both XML and JSON-LD), so provide the data with Media Fragment semantics done properly inside the universal format structuring convention. </xsl:comment>
          <media-fragments>
            <mf:name>t</mf:name>
            <mf:value>
              <xsl:value-of select="./start//text()"/>
              <xsl:text>,</xsl:text>
              <xsl:value-of select="./end//text()"/>
            </mf:value>
            <xsl:comment> Media Fragments doesn't provide sufficient semantics either as it's based on use in URIs. </xsl:comment>
            <temporal>
              <start>
                <xsl:value-of select="./start//text()"/>
              </start>
              <end>
                <xsl:value-of select="./end//text()"/>
              </end>
            </temporal>
          </media-fragments>
        </anno:selector>
      </anno:target>
    </annotation>
  </xsl:template>

  <xsl:template match="text()|node()|@*"/>

</xsl:stylesheet>
